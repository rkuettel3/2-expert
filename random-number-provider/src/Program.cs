var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/", () =>
{
    int lowerBoundary = app.Configuration.GetValue<int>("lowerBoundary", 1);
    int upperBoundary = app.Configuration.GetValue<int>("upperBoundary", 100);

    Random numberGenerator = new Random();
    int randomNumber = numberGenerator.Next(lowerBoundary, upperBoundary + 1);

    Console.WriteLine($"Generated number: {randomNumber}");

    return randomNumber;
});

app.Run();