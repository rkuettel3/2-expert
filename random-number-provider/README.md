# Random Number Provider

This simple application provides an http endpoint, on which random numbers ranging from 1 to 100 per default are delivered. These random numbers are actually for free 😆

The lower and upper boundary (both inclusive) may be configured using the configuration variables named `lowerBoundary` and `upperBoundary`.

On application start, a webserver listening on port `6301` is started. On every request towards `http://localhost:6301` a simple text body containing a random number is generated and delivered.

You may start the application using the command `dotnet run`. To provide configuration values, you may append corresponding flags:

```bash
cd src/
dotnet run --lowerBoundary 1000 --upperBoundary 9999
```