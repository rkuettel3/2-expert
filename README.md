# 2C expert

Howto run:

Execute this Commands to start the random-number-provider.

```bash
cd ./random-number-provider/src
docker build . -t random-number-provider
docker run -d --name random-number-provider random-number-provider
cd ../..
```

Then execute this command to start the game in the background:

```bash
cd ./number-guessing/src
docker build . -t number-guessing
docker run -d --name number-guessing --link random-number-provider:rng -e numberProviderHost=rng  number-guessing
```

Now you can play the game by running:

```bash
docker exec -it number-guessing /app/NumberGuessing
```
