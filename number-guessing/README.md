# Number Guessing

This is a simple application where the player has to guess a secret number.

The secret number is either requested from an http endpoint or generated randomly in case the specific http endpoint is not reachable.

## Configuration

The http endpoint to retrieve secret numbers from and the range for the secret number are configurable. Every configurable value may be given as command line argument or defined in an environment variable.

### HTTP endpoint

The http endpoint to retrieve the secret number from may be configured according to the table below.

| Key                  | Description                                                             | Sample values                                                 | Default value |
| -------------------- | ----------------------------------------------------------------------- | ------------------------------------------------------------- | ------------- |
| `numberProviderHost` | The host part of the domain where the http request is directed to.      | `127.0.0.1`, `my-number-provider.com`, `compose-service-name` | `localhost`   |
| `numberProviderPort` | The port on which the random number provider is listening for requests. | `80`, `8080`                                                  | `6301`        |

The request is sent as a http `GET` request using the `http` scheme to an endpoint following the pattern `http://[numberProviderHost]:[numberProviderPort]/`.

### Number range

The lower boundary and upper boundary for random number generation are configurable. The number range is only available for the internal, random number generator and is _not_ applied on the secret number retrieval over http.

| Key           | Description                                             | Sample values    | Default value |
| ------------- | ------------------------------------------------------- | ---------------- | ------------- |
| lowerBoundary | The smallest possible value to be generated (inclusive) | `0`, `10`, `55`  | `1`           |
| upperBoundary | The highest possible value to be generated (inclusive)  | `5`, `20`, `999` | `100`         |

## Run the Application

The application is written in C# using the .NET 8 SDK. It runs on the console:

```bash
cd src/
dotnet run
```

👉 **Proposal: Run it inside a docker container!**
