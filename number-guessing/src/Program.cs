﻿using Microsoft.Extensions.Configuration;

// Load configuration
IConfiguration configuration = new ConfigurationBuilder().AddCommandLine(args)
                                                         .AddEnvironmentVariables()
                                                         .Build();

// Define the lower and upper boundary for the generation of secret numbers.
int lowerBoundary = configuration.GetValue<int>("lowerBoundary", 1);
int upperBoundary = configuration.GetValue<int>("upperBoundary", 100);

// Get secret number.
int secretNumber = await GetSecretNumber(lowerBoundary, upperBoundary);

// Variable must be declared here (outside of do-while loop)
// to be used within the loops condition.
int userInput;

// Clear content on console.
Console.Clear();

do
{
    userInput = ReadUserInput();
    string output;

    if (userInput == secretNumber)
    {
        output = $"\nCONGRATULATIONS! The secret number is {userInput}.\n";
    }
    else
    {
        output = userInput < secretNumber
            ? $"\nTOO SMALL! The secret number is BIGGER than {userInput}.\n"
            : $"\nTOO HIGH! The secret number is SMALLER than {userInput}.\n";
    }

    // Clear console and print output.
    Console.Clear();
    Console.WriteLine(output);

} while (userInput != secretNumber);

// This functions asks the user to enter a number to guess the secret, random number.
// The input (string) is convertet to an integer value, validated and returned.
int ReadUserInput()
{
    int userInput;

    do
    {
        Console.Write($"Guess the secret number between {lowerBoundary} and {upperBoundary}: ");
        string? input = Console.ReadLine();

        // Try to parse the user input as integer value.
        if (!int.TryParse(input, out userInput) || !IsValueInValidRange(userInput))
        {
            Console.WriteLine($"\nThe given value ({input}) is invalid.");
        }
    } while (!IsValueInValidRange(userInput));

    return userInput;
}

// This function checks whether the given value is within the valid range for secret numbers.
bool IsValueInValidRange(int value)
{
    return value >= lowerBoundary && value <= upperBoundary;
}

async Task<int> GetSecretNumber(int lowerBoundary, int upperBoundary)
{
    int secretNumber;

    string numberProviderHost = configuration.GetValue<string>("numberProviderHost", "localhost")!;
    string numberProviderPort = configuration.GetValue<string>("numberProviderPort", "6301")!;

    try
    {
        HttpClient httpClient = new HttpClient { BaseAddress = new Uri($"http://{numberProviderHost}:{numberProviderPort}"), };

        using HttpResponseMessage response = await httpClient.GetAsync("/");
        response.EnsureSuccessStatusCode();

        string responseContent = await response.Content.ReadAsStringAsync();
        secretNumber = int.Parse(responseContent);
    }
    catch (Exception exception)
    {
        Console.WriteLine("Exception occured: " + exception.Message);
        secretNumber = GenerateRandomNumber(lowerBoundary, upperBoundary);
    }

    return secretNumber;
}

// Generate random number
int GenerateRandomNumber(int lowerBoundary, int upperBoundary)
{
    Random randomNumberGenerator = new Random();
    return randomNumberGenerator.Next(lowerBoundary, 1 + upperBoundary);
}